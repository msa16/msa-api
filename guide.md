graphql 설정
-----
* schema
  * query: read를 선언하고 조회 위주 선언
  * mutation: cud를 선언하고 조작 위주 선언
  * camel 표기법으로 사용
* read 설정
  * 서비스 명 + 기능으로 명칭하고 인자와 return type 정의
  * return 받을 구조 설정
  
###### EX) User Read
```
schema {
    query: read
}

type read{
    userSelectList(userSeq: Int, userId : String, limit: Int, offset: Int, orderBy: String): [userResult]
}

type userResult{
    userSeq: Int
    userId : String
    userName : String
    regDate : String
}
```
* cud 설정
  * 서비스 명 + 기능으로 명칭하고 인자 type 정의
  
###### EX) User CUD
```
schema {
    mutation: cud
}

type cud{
    userCreate(param: userCreate): Int
}

input userCreate {
    userId : String!
    userName : String!
    regDate : String
}
``` 

application.yml 설정
----
* Tree 구조로 root(서비스 명) 아래 read, cud, bulk로 구성되며 불필요 시 선언하지 않아도 됨
* snake 표기법으로 사용하며 SQL 작성과 비슷한 구조
  * read: table, fields, count를 기본으로 가지고 encFields, equal, likes, between, default를 옵션으로 가짐
  * cud: table, pk를 기본으로 가지고 default를 옵션으로 가짐
  * bulk: table, fk, excel-title-row, excel-fields을 기본으로 가지고 default를 옵션으로 가짐
  
###### EX) Board Setting
```
board:
  read:
    table: board
    count: count(*)
    fields: board_seq, title, contents, reg_name, reg_date, modify_date
    equal: board_seq, reg_name
    likes: title
    between: reg_date:sDate:eDate
    default: reg_date:NOT NULL
  cud:
    table: board
    pk: board_seq
    default:
      create: reg_date:DATE
      modify: modify_date:DATE
  bulk:
    table: board
    excel-title-row: 1
    excel-fields: title, contents
    default: reg_name:system, reg_date:DATE
``` 
 
Client에서 GQL 요청 예제
----
###### EX) User Read
```
{        
    userSelectList(userId: "admin", limit: 10, offset: 0) {
        userSeq
        userId
        userName
        regDate
    }
    userCount(userId: "admin")   
}
```

###### EX) User CUD
```
mutation cud {        
    userCreate(param: {
        userId: "admin", 
        userName: "관리자"
    })  
}
```

###### EX) User Bulk
* content-type이 multipart/form-data으로 key/value 형식
* operations에 GQL이 입력되고 fileTag가 요청되는 파일 파라미터 명
```
test_excel: {FILE}
operations: mutation cud{ boardBulk(param : { fileTag : "test_excel" }) }
```
