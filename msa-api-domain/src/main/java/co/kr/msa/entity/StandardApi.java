package co.kr.msa.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
public class StandardApi {

    public StandardApi(String tableNm){
        this.tableNm = tableNm;
    }

    //DB Table Name
    private String tableNm;

    //auto generation id
    private Integer id;

    //SELECT
    private List<String> fields;

    //INSERT, UPDATE
    private Map<String, Object> values;

    //WHERE
    private Map<String, Object> equal;
    private Map<String, Object> like;
    private Map<String, List<String>> between;
    private Map<String, List<String>> inCondition;
    private Map<String, String> is;

    private Integer offset; // offset
    private Integer limit; // limit
    private Integer page; // page

    private Map<String, String> orderBy; // 정렬

    public void addFields(String field){
        if(fields == null){
            fields = new ArrayList<String>();
        }

        fields.add(field);
    }

    public void addValues(String field, Object value){
        if(values == null){
            values = new LinkedHashMap<String, Object>();
        }
        values.put(field, value);
    }

    public void addEqual(String field, Object value){
        if(equal == null){
            equal = new LinkedHashMap<String, Object>();
        }
        equal.put(field, value);
    }

    public void addLike(String field, Object value){
        if(like == null){
            like = new LinkedHashMap<String, Object>();
        }
        like.put(field, value);
    }

    public void addBetween(String field, String val1, String val2){
        if(between == null){
            between = new LinkedHashMap<String, List<String>>();
        }

        List<String> list = new ArrayList<String>();
        list.add(val1);
        list.add(val2);

        between.put(field, list);
    }

    public void addIn(String field, List<String> values){
        if(inCondition == null){
            inCondition = new LinkedHashMap<String, List<String>>();
        }
        inCondition.put(field, values);
    }

    public void addOrderBy(String field, String value){
        if(orderBy == null){
            orderBy = new LinkedHashMap<String, String>();
        }
        orderBy.put(field, value);
    }

    public void addIs(String field, String value){
        if(is == null){
            is = new LinkedHashMap<String, String>();
        }
        is.put(field, value);
    }
}