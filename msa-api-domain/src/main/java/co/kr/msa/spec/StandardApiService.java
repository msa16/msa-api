package co.kr.msa.spec;


import co.kr.msa.entity.StandardApi;

import java.util.List;
import java.util.Map;

public interface StandardApiService {

    List<Map<String, Object>> selectList(StandardApi standardApi);

    Map<String, Object> selectOne(StandardApi standardApi);

    Integer count(StandardApi standardApi);

    Integer create(StandardApi standardApi);

    Integer modify(StandardApi standardApi);

    Integer remove(StandardApi standardApi);

    Integer bulk(StandardApi standardApi);

    Integer towInsert(List<StandardApi> list1, List<StandardApi> list2, List<String> foreignKey);
}