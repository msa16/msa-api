package co.kr.msa.rest;

import co.kr.msa.constants.Constants;
import co.kr.msa.fetcher.*;
import co.kr.msa.spec.StandardApiService;

import co.kr.msa.util.FileUtil;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.language.FieldDefinition;
import graphql.language.ObjectTypeDefinition;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.util.*;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Slf4j
@CrossOrigin("*")
@RequestMapping("${uri.api}/${uri.version}/${uri.resource}")
@RestController
public class StandardApiResource {

    private final StandardApiService standardApiService;

    private final Environment env;

    public StandardApiResource(StandardApiService standardApiService, Environment env) {
        this.standardApiService = standardApiService;
        this.env = env;
    }

    // 파일 저장 위치
    @Value("${file.path}")
    private String filePath;

    // .graphql 파일 위치
    @Value("${graphql.path}")
    private String graphQlPath;

    // .graphql 파일 명
    @Value("${graphql.file}")
    private String graphQlFile;

    private GraphQL graphQL;

    @PostConstruct
    public void loadSchema() {
        File schemaFile = new File(graphQlPath + "/" + graphQlFile);
        TypeDefinitionRegistry registry = new SchemaParser().parse(schemaFile);

        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(registry, buildWiring(registry));
        graphQL = GraphQL.newGraphQL(schema).build();

        log.info("Add GraphQL File: " + graphQlFile);
    }

    // DataFetcher(Resolver) Mapping
    private RuntimeWiring buildWiring(TypeDefinitionRegistry registry){

        Map<String, DataFetcher> readFetchers = getDateFetcherMap("read", registry);
        Map<String, DataFetcher> cudFetchers = getDateFetcherMap("cud", registry);

        return RuntimeWiring.newRuntimeWiring()
                .type("read", typeWriting -> typeWriting.dataFetchers(readFetchers))
                .type("cud", typeWriting -> typeWriting.dataFetchers(cudFetchers))
                .build();
    }

    private Map<String, DataFetcher> getDateFetcherMap(String type, TypeDefinitionRegistry registry){

        Map<String, DataFetcher> result = new LinkedHashMap<String, DataFetcher>();
        ObjectTypeDefinition definition = (ObjectTypeDefinition) registry.getType(type).orElse(new ObjectTypeDefinition(""));

        for(FieldDefinition fd : definition.getFieldDefinitions()){
            String fetcherNm = fd.getName();

            //default read
            if(fetcherNm.contains(Constants.LIST)){
                String prefix = fetcherNm.replace(Constants.LIST,"");
                result.put(prefix + Constants.LIST, new ListFetcher(standardApiService, env, prefix));
            } else if(fetcherNm.contains(Constants.ONE)){
                String prefix = fetcherNm.replace(Constants.ONE,"");
                result.put(prefix + Constants.ONE, new OneFetcher(standardApiService, env, prefix));
            } else if(fetcherNm.contains(Constants.COUNT)){
                String prefix = fetcherNm.replace(Constants.COUNT,"");
                result.put(prefix + Constants.COUNT, new CountFetcher(standardApiService, env, prefix));

            //default cud
            } else if(fetcherNm.contains(Constants.CREATE)){
                String prefix = fetcherNm.replace(Constants.CREATE,"");
                result.put(prefix + Constants.CREATE, new CreateFetcher(standardApiService, env, prefix));
            } else if(fetcherNm.contains(Constants.MODIFY)){
                String prefix = fetcherNm.replace(Constants.MODIFY,"");
                result.put(prefix + Constants.MODIFY, new ModifyFetcher(standardApiService, env, prefix));
            } else if(fetcherNm.contains(Constants.REMOVE)){
                String prefix = fetcherNm.replace(Constants.REMOVE,"");
                result.put(prefix + Constants.REMOVE, new RemoveFetcher(standardApiService, env, prefix));
            } else if(fetcherNm.contains(Constants.BULK)){
                String prefix = fetcherNm.replace(Constants.BULK,"");
                result.put(prefix + Constants.BULK, new BulkFetcher(standardApiService, env, prefix));
            } else {
                log.warn("not defined type ==> " + fetcherNm);
            }
        }

        return result;
    }

    /**
     * Client 요청/응답 부분
     *
     * @param request: Object 형식
     * @return 성공: .graphql에 작성된 Result 값, 실패: Error 메시지
     */
    @PostMapping
    public ResponseEntity<Object> standardGraphQl(HttpServletRequest request) {

        ExecutionResult result = null;
        HttpStatus status = null;
        String contentType = request.getHeader("Content-Type");

        log.info("request resource: " + env.getProperty("uri.resource") + ", Content-Type: " + contentType);
        log.debug("request: " + request);
        try{
            String queryTagName = "operations";
            String query;

            //첨부파일이 있을경우 서버 저장 후 Query에 경로 주입
            if(ServletFileUpload.isMultipartContent(request)){
                MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest) request;
                MultipartFile mf = mhsr.getFile(queryTagName);

                //operation의 Contents Type이 Mulitpart가 아님을 고려
                if(mf == null){
                    Part part = request.getPart(queryTagName);
                    if(part == null){
                        throw new NullPointerException("not exist operations.");
                    }
                    log.trace("[" + queryTagName + "] Contents Type " + part.getContentType());
                    query = IOUtils.toString(part.getInputStream());
                } else {
                    log.trace("[" + queryTagName + "] Contents Type " + mf.getContentType());
                    query = IOUtils.toString(mf.getInputStream());
                }

                Iterator<String> itr = mhsr.getFileNames();
                while(itr.hasNext()){
                    String fileTag = itr.next();
                    log.debug("fileTag : " + fileTag);

                    mf = mhsr.getFile(fileTag);
                    if(!fileTag.equals(queryTagName)) {
                        String savePath = FileUtil.saveFile(filePath, mf.getOriginalFilename(), mf.getInputStream());
                        query = query.replaceAll(fileTag, savePath);
                    }
                }

            } else {
                query = IOUtils.toString(request.getReader());
            }

            log.debug("Query : " + query);
            result = graphQL.execute(query);

            status = HttpStatus.OK;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<Object>(result, status);
    }
}
