package co.kr.msa.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.InputStream;

public class FileUtil {

    public static String saveFile(String path, String fileName ,InputStream fileStream) throws Exception{

        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdir();
        }

        String fullPath = path + "/" + fileName;
        File targetFile = new File(fullPath);
        FileUtils.copyInputStreamToFile(fileStream, targetFile);

        return fullPath;
    }

    public static void removeFile(String path) {

        File file = new File(path);

        if (file.exists()) {
            file.delete();
        }
    }

}
