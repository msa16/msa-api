package co.kr.msa.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.Cipher;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.util.Base64;

@Slf4j
public class EncryptUtil {
    private Key privateKey;
    private Cipher cipher;

    public EncryptUtil(Environment env){
        try{
            String keystorePath = env.getProperty("security.keystore.path");
            String alias = env.getProperty("security.keystore.alias");
            String keyPwd = env.getProperty("security.keystore.passwd");

            InputStream is = new FileInputStream(new File(keystorePath));
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(is, keyPwd.toCharArray());

            privateKey = keystore.getKey(alias, keyPwd.toCharArray());
            cipher = Cipher.getInstance("AES");
        } catch (Exception e) {
            log.error("AESUtil Init Error!", e);
        }
    }

    public String getEncData(String data) {
        try{
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] encrypted = cipher.doFinal(data.getBytes());

            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getDecData(String data) {
        try{
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(data));

            return new String(decrypted);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getHash(String data) {
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return passwordEncoder.encode(data);
    }
}
