package co.kr.msa.util;

public class CamelUtil {

    /**
     * 1. _가 있으면 CamelCase로 변경
     * 2. _가 없으면 SnakeCase로 변경
     */
    public static String convert(String val){
        String result;

        if(val.indexOf("_") > 0){
            StringBuilder sb = new StringBuilder();
            boolean nextIsUpper = false;
            if (val != null && val.length() > 0) {
                if (val.length() > 1 && val.charAt(1) == '_') {
                    sb.append(Character.toUpperCase(val.charAt(0)));
                } else {
                    sb.append(Character.toLowerCase(val.charAt(0)));
                }

                for(int i = 1; i < val.length(); ++i) {
                    char c = val.charAt(i);
                    if (c == '_') {
                        nextIsUpper = true;
                    } else if (nextIsUpper) {
                        sb.append(Character.toUpperCase(c));
                        nextIsUpper = false;
                    } else {
                        sb.append(Character.toLowerCase(c));
                    }
                }
            }
            result = sb.toString();

        } else {
            String[] values = val.split("(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])");

            for (int i = 0, len = values.length; i < len; i++) {
                values[i] = values[i].toLowerCase();
            }
            result =  String.join("_", values);
        }

        return result;
    }
}
