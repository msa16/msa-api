package co.kr.msa.util;

import java.util.HashMap;

public class ResultMap extends HashMap {

    /**
     * Mybatis의 Result를 Map으로 반환할때 Field 명이 SnakeCase면 CamelCase로 반환하여 Key를 사용
     */
    @Override
    public Object put(Object key, Object value){
        return super.put(CamelUtil.convert((String)key), value);
    }
}
