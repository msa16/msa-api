package co.kr.msa.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class RestApiTemplate {

    private final String AUTH_NAME = "authorization";

    public ResponseEntity<Object> requestPost(String url, String token, String body){

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTH_NAME, token);
        HttpEntity<?> entity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> result = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);

        log.info(result.toString());


        return result;
    }

 }
