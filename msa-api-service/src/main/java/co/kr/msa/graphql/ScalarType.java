package co.kr.msa.graphql;

import graphql.schema.Coercing;
import graphql.schema.GraphQLScalarType;
import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.Part;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ScalarType {

    public final GraphQLScalarType FILE = new GraphQLScalarType("File", "The custom scalar type named File", new Coercing() {

        @Override
        public Object serialize(Object o) {
            throw new RuntimeException("Upload is an input-only type");
        }

        @Override
        public Object parseValue(Object input) {
            if (input instanceof Part) {
                Part part = (Part) input;
                try {
                    Map<String, String> map = new HashMap<String, String>();
                    String fileName = part.getName();
                    String file = new String(Base64.encodeBase64String(new byte[part.getInputStream().available()]));
                    map.put("fileName", fileName);
                    map.put("file", file);
                    part.delete();

                    return map;
                } catch (IOException e) {
                    throw new RuntimeException("Couldn't read content of the uploaded file");
                }
            } else if (null == input) {
                return null;
            } else {
                throw new RuntimeException("Expected type " + Part.class.getName() + " but was " + input.getClass().getName());
            }
        }

        @Override
        public Object parseLiteral(Object o) {
            throw new RuntimeException("Must use variables to specify Upload values");
        }
    });
}
