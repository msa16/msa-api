package co.kr.msa.graphql.vo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GraphqlQuery {

    private String queryName;

    private List<String> fields = new ArrayList<String>();

    private Map<String, Object> param = new LinkedHashMap<String, Object>();

    private Map<String, Object> values = new LinkedHashMap<String, Object>();

    public GraphqlQuery(String queryName){
        this.queryName = queryName;
    }

    public void addField(String field){
        fields.add(field);
    }

    public void addParam(String key, Object value){
        param.put(key, value);
    }

    public void addValues(String key, Object value){
        values.put(key, value);
    }

    private void checkType(StringBuilder sb, Object o){
        if(o instanceof Integer){
            sb.append((int) o);
        } else {
            sb.append("\"" + o + "\"");
        }
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\r\n" + queryName);

        if(param.keySet().size() > 0) {
            sb.append("(");

            int i = 0;
            for(String k : param.keySet()){
                sb.append(k + ":");
                checkType(sb, param.get(k));

                if(i != (param.keySet().size() - 1)){
                    sb.append(", ");
                }
                i++;
            }
            sb.append(")");
        }


        if(values.keySet().size() > 0) {
            sb.append("(");
            sb.append("\r\n\tparam: {");
            for(String k : values.keySet()){
                sb.append("\r\n\t\t" + k + ":");
                checkType(sb, values.get(k));
            }
            sb.append("\r\n\t})");
        }

        if(fields.size() > 0) {
            sb.append("{");
            for (String field : fields) {
                sb.append("\r\n\t" + field);
            }
            sb.append("\r\n}");
        }

        return sb.toString();
    }


    public static void main(String[] ar){

        GraphqlQuery gq = new GraphqlQuery("SelectList");
        gq.addParam("code", "A00000000");
        gq.addParam("limit", 10);
        gq.addParam("offset", 0);

        gq.addField("code");
        gq.addField("codeNm");
        gq.addField("codeLocalNm");

        System.out.println(gq.toString());
        System.out.println();

        GraphqlQuery gq1 = new GraphqlQuery("Create");
        gq1.addValues("code", "T001");
        gq1.addValues("codeNm", "테스트");
        gq1.addValues("codeLocalNm", "서울시");

        System.out.println(gq1.toString());
    }

}
