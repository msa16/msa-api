package co.kr.msa.graphql.vo;

import java.util.ArrayList;
import java.util.List;

public class GraphqlRequest {

    public enum QueryType {
        query,
        mutation
    }

    private QueryType queryType;

    private List<GraphqlQuery> queries = new ArrayList<GraphqlQuery>();

    public GraphqlRequest(QueryType queryType){
        this.queryType = queryType;
    }

    public void addQuery(GraphqlQuery query){
        queries.add(query);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        if(QueryType.query == queryType){
            sb.append("query read{");
        } else {
            sb.append("mutation cud{");
        }

        for(GraphqlQuery q : queries){
            sb.append(q.toString());
        }

        sb.append("\r\n}");
        return sb.toString();
    }


    public static void main(String[] ar){
        GraphqlRequest gr = new GraphqlRequest(QueryType.query);

        GraphqlQuery gq = new GraphqlQuery("SelectList");
        gq.addParam("code", "A00000000");
        gq.addParam("limit", 10);
        gq.addParam("offset", 0);
        gq.addField("code");
        gq.addField("codeNm");
        gq.addField("codeLocalNm");
        gr.addQuery(gq);

        GraphqlQuery count = new GraphqlQuery("count");
        count.addParam("code", "A00000000");
        gr.addQuery(count);

        System.out.println(gr.toString());
        System.out.println();

        GraphqlRequest gr1 = new GraphqlRequest(QueryType.mutation);
        GraphqlQuery gq1 = new GraphqlQuery("Create");
        gq1.addValues("code", "T001");
        gq1.addValues("codeNm", "테스트");
        gq1.addValues("codeLocalNm", "서울시");

        gr1.addQuery(gq1);

        GraphqlQuery update = new GraphqlQuery("Update");
        update.addValues("code", "T001");
        update.addValues("remark", "TEST!");

        gr1.addQuery(update);

        System.out.println(gr1.toString());
    }

}
