package co.kr.msa.constants;

public class Constants {

    // Query(SelectList) 요청 시 Pagination 및 정렬
    public static String LIMIT = "limit";
    public static String OFFSET = "offset";
    public static String PAGE = "page";
    public static String ORDER_BY = "orderBy";

    // GraphQL 기본 Type
    public static String LIST = "List";
    public static String ONE = "One";
    public static String COUNT = "Count";
    public static String CREATE = "Create";
    public static String MODIFY = "Modify";
    public static String REMOVE = "Remove";
    public static String BULK = "Bulk";

}
