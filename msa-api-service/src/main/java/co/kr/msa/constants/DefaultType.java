package co.kr.msa.constants;

// Mutation(Create, Modify) 요청 시 application.yml에 미리 등록된 기본값
public enum DefaultType {
    DATE,
    DATE_STR,
    ID,
    UUID
}