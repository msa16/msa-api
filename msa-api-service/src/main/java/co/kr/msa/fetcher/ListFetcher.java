package co.kr.msa.fetcher;

import co.kr.msa.constants.Constants;
import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.CamelUtil;
import co.kr.msa.util.EncryptUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Map;

@Slf4j
public class ListFetcher extends Fetcher implements DataFetcher<List<Map<String, Object>>> {

    public ListFetcher(StandardApiService standardApiService, Environment env, String queryName){
        this.standardApiService = standardApiService;
        this.env = env;
        this.queryName = queryName;

        //data 암호화 사용여부 체크
        if(env.containsProperty(queryName + ".read.encFields")){
            isEncrypt = true;
            encryptUtil = new EncryptUtil(env);
            encFields = env.getProperty(queryName + ".read.encFields", List.class);
        }
    }

    @Override
    public List<Map<String, Object>> get(DataFetchingEnvironment data) {
        try {
            //조건 정리
            StandardApi standardApi = getStandardApi(data);
            //Store 호출
            List<Map<String, Object>> result = standardApiService.selectList(standardApi);

            //데이터 복호화
            if(isEncrypt){
                for(Map<String, Object> map : result){
                    getDecMap(map, encFields, encryptUtil);
                }
            }

            return result;

        } catch (Exception e){
            log.error(e.toString(), e);
            throw e;
        }
    }

    private StandardApi getStandardApi(DataFetchingEnvironment data) {

        StandardApi standardApi = new StandardApi(getTableNm(queryName + ".read.table"));

        //필드
        setFields(standardApi, queryName + ".read.fields");

        //equal 조건
        setEqual(standardApi, queryName + ".read.equal", data);

        //암호화된 데이터 equal 조건
        setEncEqual(standardApi);

        //like 조건
        setLike(standardApi, queryName + ".read.likes", data);

        //between 조건
        setBetween(standardApi, queryName + ".read.between", data);

        //In 조건
        setIn(standardApi, queryName + ".read.in", data);

        // default 조건
        setDefault(standardApi, queryName + ".read.default");

        if(isExistData(Constants.LIMIT, data)){
            standardApi.setLimit(data.getArgument(Constants.LIMIT));
        }
        if(isExistData(Constants.OFFSET, data)){
            standardApi.setOffset(data.getArgument(Constants.OFFSET));
        }
        if(isExistData(Constants.PAGE, data)){
            standardApi.setPage(data.getArgument(Constants.PAGE));
        }
        if(isExistData(Constants.ORDER_BY, data)){
            String orders = data.getArgument(Constants.ORDER_BY);
            for(String strOrder : orders.split(",")) {
                String[] arrSort = strOrder.split(":");
                standardApi.addOrderBy(CamelUtil.convert(arrSort[0]), arrSort[1]);
            }
        }

        log.debug("[" + queryName + "] SelectList getStandardApi: " + standardApi.toString());

        return standardApi;
    }

}
