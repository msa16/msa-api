package co.kr.msa.fetcher;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.CamelUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Map;

@Slf4j
public class RemoveFetcher extends Fetcher implements DataFetcher<Integer> {


    public RemoveFetcher(StandardApiService standardApiService, Environment env, String queryName){
        this.standardApiService = standardApiService;
        this.env = env;
        this.queryName = queryName;
    }

    @Override
    public Integer get(DataFetchingEnvironment data) {
        try {
            StandardApi standardApi = getStandardApi(data);
            return standardApiService.remove(standardApi);
        } catch (Exception e){
            log.error(e.toString(), e);
            throw e;
        }
    }

    private StandardApi getStandardApi(DataFetchingEnvironment data) {
        StandardApi standardApi = new StandardApi(getTableNm(queryName + ".cud.table"));

        Map<String, Object> param = data.getArgument("param");

        List<String> list = env.getProperty(queryName + ".cud.pk", List.class);
        for(String pk : list){
            standardApi.addEqual(pk, param.get(CamelUtil.convert(pk)));
        }

        log.debug("[" + queryName + "] Remove getStandardApi: " + standardApi.toString());

        return standardApi;
    }

}
