package co.kr.msa.fetcher;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.EncryptUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.util.List;

@Slf4j
public class CountFetcher extends Fetcher implements DataFetcher<Integer> {

    public CountFetcher(StandardApiService standardApiService, Environment env, String queryName){
        super.standardApiService = standardApiService;
        super.env = env;
        super.queryName = queryName;

        //data 암호화 사용여부 체크
        if(env.containsProperty(queryName + ".read.encFields")){
            isEncrypt = true;
            encryptUtil = new EncryptUtil(env);
            encFields = env.getProperty(queryName + ".read.encFields", List.class);
        }
    }

    @Override
    public Integer get(DataFetchingEnvironment data) {
        try {
            StandardApi standardApi = getStandardApi(data);
            return standardApiService.count(standardApi);
        } catch (Exception e){
            log.error(e.toString(), e);
            throw e;
        }
    }

    private StandardApi getStandardApi(DataFetchingEnvironment data) {
        StandardApi standardApi = new StandardApi(getTableNm(queryName + ".read.table"));

        String field = env.getProperty(queryName + ".read.count");
        standardApi.addFields(field);

        //equal 조건
        setEqual(standardApi, queryName + ".read.equal", data);

        //암호화된 데이터 equal 조건
        setEncEqual(standardApi);

        //like 조건
        setLike(standardApi, queryName + ".read.likes", data);

        //between 조건
        setBetween(standardApi, queryName + ".read.between", data);

        //In 조건
        setIn(standardApi, queryName + ".read.in", data);

        // default 조건
        setDefault(standardApi, queryName + ".read.default");

        log.debug("[" + queryName + "] Count getStandardApi: " + standardApi.toString());

        return standardApi;
    }

}
