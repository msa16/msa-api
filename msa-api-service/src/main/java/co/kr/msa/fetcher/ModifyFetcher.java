package co.kr.msa.fetcher;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.CamelUtil;
import co.kr.msa.util.EncryptUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Map;

@Slf4j
public class ModifyFetcher extends Fetcher implements DataFetcher<Integer> {

    public ModifyFetcher(StandardApiService standardApiService, Environment env, String queryName){
        this.standardApiService = standardApiService;
        this.env = env;
        this.queryName = queryName;

        //data 암호화 사용여부 체크
        if(env.containsProperty(queryName + ".cud.encFields")
                || env.containsProperty(queryName + ".cud.hashFields")){
            isEncrypt = true;
            encryptUtil = new EncryptUtil(env);

            if(env.containsProperty(queryName + ".cud.encFields")){
                encFields = env.getProperty(queryName + ".cud.encFields", List.class);
            }
            if(env.containsProperty(queryName + ".cud.hashFields")){
                hashFields = env.getProperty(queryName + ".cud.hashFields", List.class);
            }
        }
    }

    @Override
    public Integer get(DataFetchingEnvironment data) {
        try {
            StandardApi standardApi = getStandardApi(data);
            return standardApiService.modify(standardApi);
        } catch (Exception e){
            log.error(e.toString(), e);
            throw e;
        }
    }

    private StandardApi getStandardApi(DataFetchingEnvironment data) {
        StandardApi standardApi = new StandardApi(getTableNm(queryName + ".cud.table"));

        Map<String, Object> param = data.getArgument("param");
        setValues(standardApi, param, queryName + ".cud.default.modify");

        List<String> list = env.getProperty(queryName + ".cud.pk", List.class);
        for(String pk : list){
            standardApi.getValues().remove(pk);
            standardApi.addEqual(pk, param.get(CamelUtil.convert(pk)));
        }

        log.debug("[" + queryName + "] Modify getStandardApi: " + standardApi.toString());

        return standardApi;
    }

}
