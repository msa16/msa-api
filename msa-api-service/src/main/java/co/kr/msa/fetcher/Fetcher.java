package co.kr.msa.fetcher;

import co.kr.msa.constants.DefaultType;
import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.CamelUtil;
import co.kr.msa.util.EncryptUtil;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class Fetcher {

    protected StandardApiService standardApiService;

    protected Environment env;

    protected String queryName;

    protected EncryptUtil encryptUtil;

    protected boolean isEncrypt = false;

    protected List<String> encFields;

    protected List<String> hashFields;

    protected String getTableNm(String Key){
        return env.getProperty(Key);
    }

    protected void setValues(StandardApi standardApi, Map<String, Object> param, String props){
        if(isEncrypt){
            for(String key : param.keySet()){
                String field = CamelUtil.convert(key);
                Object rawData = param.get(key);
                if(encFields != null && encFields.size() > 0 && encFields.contains(field)){
                    standardApi.addValues(field, encryptUtil.getEncData((String)rawData));
                } else if(hashFields != null && hashFields.size() > 0 && hashFields.contains(field)){
                    standardApi.addValues(field, encryptUtil.getHash((String)rawData));
                } else {
                    standardApi.addValues(field, rawData);
                }
            }
        } else {
            for(String key : param.keySet()){
                String field = CamelUtil.convert(key);
                standardApi.addValues(field, param.get(key));
            }
        }

        if(env.containsProperty(props)){
            List<String> defaults = env.getProperty(props, List.class);
            for(String def : defaults){
                String[] arr = def.split(":");
                if(arr.length != 2){
                    log.error("invalid create default format: " + def);
                    continue;
                }

                if(!standardApi.getValues().containsKey(arr[0])){
                    standardApi.addValues(arr[0], getDefaultValue(arr[1]));
                }
            }
        }

    }

    protected void setFields(StandardApi standardApi, String props){
        List<String> list = env.getProperty(props, List.class);
        for(String field : list){
            standardApi.addFields(field);
        }
    }

    protected void setEqual(StandardApi standardApi, String props, DataFetchingEnvironment data){
        if(env.containsProperty(props)){
            List<String> list = env.getProperty(props, List.class);
            for(String equal : list){
                String field = removeAlias(equal);
                if(isExistData(field, data)){
                    standardApi.addEqual(equal, data.getArgument(field));
                }
            }
        }
    }

    protected void setEncEqual(StandardApi standardApi){
        if(isEncrypt && standardApi.getEqual() != null){
            for(String field : encFields){
                if(standardApi.getEqual().containsKey(field)){
                    String val = (String) standardApi.getEqual().get(field);
                    standardApi.getEqual().replace(field, encryptUtil.getEncData(val));
                }
            }
        }
    }

    protected void setLike(StandardApi standardApi, String props, DataFetchingEnvironment data){
        if(env.containsProperty(props)){
            List<String> list = env.getProperty(props, List.class);
            for(String like : list){
                String field = removeAlias(like);
                if(isExistData(field, data)){
                    standardApi.addLike(like, data.getArgument(field) + "%");
                }
            }
        }
    }

    protected void setBetween(StandardApi standardApi, String props, DataFetchingEnvironment data){

        if(env.containsProperty(props)){
            List<String> list = env.getProperty(props, List.class);
            for(String between : list){
                String[] arr = between.split(":");

                if(arr.length != 3){
                    log.error("invalid between format: " + between);
                    continue;
                }

                if(isExistData(arr[1], data) && isExistData(arr[2], data)){
                    standardApi.addBetween(arr[0], data.getArgument(arr[1]), data.getArgument(arr[2]));
                }
            }
        }
    }

    protected void setIn(StandardApi standardApi, String props, DataFetchingEnvironment data){
        if(env.containsProperty(props)){
            List<String> list = env.getProperty(props, List.class);
            for(String field : list){
                String key = removeAlias(field);
                if(isExistData(key, data)){
                    List<String> in = new ArrayList<String>();
                    String values = data.getArgument(key);
                    String[] arr = values.split(",");

                    for (String value: arr) {
                        in.add(value);
                    }

                    standardApi.addIn(field, in);
                }
            }
        }
    }

    protected void setDefault(StandardApi standardApi, String props){
        if(env.containsProperty(props)){
            List<String> defaults = env.getProperty(props, List.class);
            for(String def : defaults){
                String[] arr = def.split(":");
                if(arr.length != 2){
                    log.error("invalid create default format: " + def);
                    continue;
                }

                if(arr[1].toLowerCase().indexOf("null") > -1){
                    standardApi.addIs(arr[0], arr[1]);
                } else{
                    standardApi.addEqual(arr[0], arr[1]);
                }

            }
        }
    }

    //Client에서 받은 파라미터 존재 여부 확인
    protected boolean isExistData(String props, DataFetchingEnvironment data){
        boolean result = false;
        if(data.containsArgument(props)
                && data.getArgument(props) != null && !"".equals(data.getArgument(props))){
            result = true;
        }

        return result;
    }

    //application.yml에 설정된 파라미터. 테이블 별칭의 유무 확인 후 CamelCase 적용하여 반환
    protected String removeAlias(String field){
        String result;
        if(field.indexOf(".") > 0){
            String[] arr = field.split("\\.");
            result = CamelUtil.convert(arr[1]);
        } else {
            result = CamelUtil.convert(field);
        }

        return result;
    }

    //암호화된 데이터 복호화
    protected Map<String, Object> getDecMap(Map<String, Object> map, List<String> encFields, EncryptUtil encryptUtil){
        for(String field : encFields) {
            String fieldNm = CamelUtil.convert(field);
            String decData = encryptUtil.getDecData((String) map.get(fieldNm));
            map.replace(fieldNm, decData);
        }

        return map;
    }

    //application.yml에 설정된 Insert, Update 시 기본 값. 정의된 내용이 없으면 그대로 반환
    protected Object getDefaultValue(String val){
        if(DefaultType.ID.name().equals(val)){
            return "system";
        } else if(DefaultType.DATE.name().equals(val)){
            return new Date();
        } else if(DefaultType.DATE_STR.name().equals(val)){
            Calendar _calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            return dateFormat.format(_calendar.getTime());
        } else if(DefaultType.UUID.name().equals(val)){
            return UUID.randomUUID().toString().replaceAll("-","");
        }else {
            return val;
        }
    }
}
