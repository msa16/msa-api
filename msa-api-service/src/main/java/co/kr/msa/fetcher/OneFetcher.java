package co.kr.msa.fetcher;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.EncryptUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Map;

@Slf4j
public class OneFetcher extends Fetcher implements DataFetcher<Map<String, Object>> {


    public OneFetcher(StandardApiService standardApiService, Environment env, String queryName){
        this.standardApiService = standardApiService;
        this.env = env;
        this.queryName = queryName;

        //data 암호화 사용여부 체크
        if(env.containsProperty(queryName + ".read.encFields")){
            isEncrypt = true;
            encryptUtil = new EncryptUtil(env);
            encFields = env.getProperty(queryName + ".read.encFields", List.class);
        }
    }

    @Override
    public Map<String, Object> get(DataFetchingEnvironment data) {
        try {
            //조건 정리
            StandardApi standardApi = getStandardApi(data);
            //Store 호출
            Map<String, Object> result = standardApiService.selectOne(standardApi);

            //데이터 복호화
            if(isEncrypt){
                getDecMap(result, encFields, encryptUtil);
            }

            return result;
        } catch (Exception e){
            log.error(e.toString(), e);
            throw e;
        }
    }

    private StandardApi getStandardApi(DataFetchingEnvironment data) {

        StandardApi standardApi = new StandardApi(getTableNm(queryName + ".read.table"));

        //필드
        setFields(standardApi, queryName + ".read.fields");

        //equal 조건
        setEqual(standardApi, queryName + ".read.equal", data);

        //like 조건, SelectOne에서는 like조건을 equal로 처리.
        setEqual(standardApi, queryName + ".read.likes", data);

        // 데이터가 암호화된 equal 조건
        setEncEqual(standardApi);

        log.debug("[" + queryName + "] SelectOne getStandardApi: " + standardApi.toString());

        return standardApi;
    }

}
