package co.kr.msa.fetcher;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.util.FileUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import org.springframework.core.env.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BulkFetcher extends Fetcher implements DataFetcher<Integer> {

    public BulkFetcher(StandardApiService standardApiService, Environment env, String queryName){
        this.standardApiService = standardApiService;
        this.env = env;
        this.queryName = queryName;
    }

    @Override
    public Integer get(DataFetchingEnvironment data) {
        try {
            List<String> tables = env.getProperty(queryName + ".bulk.table", List.class);
            List<String> fields = env.getProperty(queryName + ".bulk.excel-fields", List.class);
            List<String> defaults = env.getProperty(queryName + ".bulk.default", List.class);
            List<Object> defaultVal = new ArrayList<Object>();

            if(tables.size() == 1) { // Single Table

                // Model init & Set Filed info
                StandardApi standardApi = new StandardApi(tables.get(0));
                standardApi.setFields(fields);

                // Add default value in model
                for(String def :defaults){
                    String[] arr = def.split(":");
                    if(arr.length != 2){
                        log.warn("invalid bulk default format: " + def);
                        continue;
                    }
                    standardApi.addFields(arr[0]);
                    defaultVal.add(getDefaultValue(arr[1]));
                }

                // Parsing Excel
                standardApi.setValues(getExcelData(data, defaultVal));

                // Bulk Store & Return result
                return standardApiService.bulk(standardApi);

            } else if(tables.size() == 2) { // Tow Tables

                // Parse table info
                String[] arr1 = tables.get(0).split("\\.");
                String[] arr2 = tables.get(1).split("\\.");

                String alias1 = arr1[0];
                String alias2 = arr2[0];
                String tableNm1 = arr1[1];
                String tableNm2 = arr2[1];
                List<String> foreignKey = env.getProperty(queryName + ".bulk.fk", List.class);

                // Parsing Excel
                Map<String, Object> excelData = getExcelData(data, defaultVal);

                List<StandardApi> list1 = new ArrayList<StandardApi>();
                List<StandardApi> list2 = new ArrayList<StandardApi>();

                // Bulk Store & Return result
                for(Object obj : excelData.values()){

                    List<Object> list = (List<Object>) obj;

                    // Model init & Set Filed info
                    StandardApi standardApi1 = new StandardApi(tableNm1);
                    StandardApi standardApi2 = new StandardApi(tableNm2);

                    for(int i = 0 ; i < fields.size() ; i++){
                        String field = fields.get(i);

                        //check field format
                        String[] fieldInfo = field.split("\\.");
                        if(fieldInfo.length != 2){
                            throw new Exception("wrong format: [" + field + "]");
                        }

                        //check field alias
                        if(fieldInfo[0].equals(alias1)){
                            standardApi1.addValues(fieldInfo[1], list.get(i));
                        } else if (fieldInfo[0].equals(alias2)){
                            standardApi2.addValues(fieldInfo[1], list.get(i));
                        } else {
                            throw new Exception("undefined alias: [" + field + "]");
                        }
                    }

                    // Add default value in model
                    for(String def : defaults){
                        String[] defInfo = def.split(":");
                        if(defInfo.length != 2){
                            log.warn("invalid bulk default format: " + def);
                            continue;
                        }

                        //check default field format
                        String[] fieldDef = defInfo[0].split("\\.");
                        if(fieldDef.length != 2){
                            throw new Exception("wrong format: [" + defInfo[0] + "]");
                        }

                        //check default field alias
                        if(fieldDef[0].equals(alias1)){
                            standardApi1.addValues(fieldDef[1], getDefaultValue(defInfo[1]));
                        } else if (fieldDef[0].equals(alias2)){
                            standardApi2.addValues(fieldDef[1], getDefaultValue(defInfo[1]));
                        } else {
                            throw new Exception("undefined alias: [" + fieldDef[0] + "]");
                        }
                    }
                    list1.add(standardApi1);
                    list2.add(standardApi2);

                }

                return standardApiService.towInsert(list1, list2, foreignKey);

            } else {
                throw new Exception("More than two tables are not supported.");
            }
        } catch (Exception e){
            log.error(e.toString(), e);
            return -1;
        }
    }

    private Map<String, Object> getExcelData(DataFetchingEnvironment data, List<Object> defaultVal) throws Exception {

        String fileTagName = env.getProperty(queryName + ".bulk.tag", "fileTag");
        Map<String, Object> param = data.getArgument("param");

        if(!param.containsKey(fileTagName)){
            throw new NullPointerException("Not exist param! key=" + fileTagName);
        }
        String filePath = (String) param.get(fileTagName);

        Map<String, Object> result = new LinkedHashMap<String, Object>();
        try{
            int titleRow = env.getProperty(queryName + ".bulk.excel-title-row", Integer.class);

            // 1. File 읽기
            InputStream inputStream = new FileInputStream(new File(filePath));

            // 2. 엑셀파일을 관리하는 객체로 받아오기 (HSSFWorkbook : .xls, XSSFWorkbook : .xlsx)
            Workbook wb = WorkbookFactory.create(inputStream); // 통합.

            // 3. 인덱스로 원하는 시트 가져오기
            Sheet sheet = wb.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();

            // 제목줄 넘기기
            for(int i = 0 ; i < titleRow ; i++){
                rows.next();
            }

            int idx = 0;
            while (rows.hasNext()) {

                // row 읽기(xlxs 만 지원)
                XSSFRow row = (XSSFRow) rows.next();
                Iterator<Cell> cells = row.cellIterator();

                List<Object> r = new ArrayList<Object>();
                while (cells.hasNext()) {
                    DataFormatter formatter = new DataFormatter();
                    // cell 읽기
                    XSSFCell cell = (XSSFCell) cells.next();
                    r.add(formatter.formatCellValue(cell));
                }
                r.addAll(defaultVal);
                result.put(String.valueOf(idx++), r);
            }

            return result;

        } catch (Exception e) {
            throw e;
        } finally {
            //최종 업로드된 파일은 삭제
            FileUtil.removeFile(filePath);
        }
    }
}
