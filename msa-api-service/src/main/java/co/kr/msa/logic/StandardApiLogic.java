package co.kr.msa.logic;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.spec.StandardApiService;
import co.kr.msa.store.StandardApiStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class StandardApiLogic implements StandardApiService {

    private final StandardApiStore standardApiStore;

    public StandardApiLogic(StandardApiStore standardApiStore) {
        this.standardApiStore = standardApiStore;
    }

    @Override
    public List<Map<String, Object>> selectList(StandardApi standardApi) {
        return standardApiStore.selectList(standardApi);
    }

    @Override
    public Map<String, Object> selectOne(StandardApi standardApi) {
        return standardApiStore.selectOne(standardApi);
    }

    @Override
    public Integer count(StandardApi standardApi) {
        return standardApiStore.count(standardApi);
    }

    @Override
    public Integer create(StandardApi standardApi) {
        return standardApiStore.create(standardApi);
    }

    @Override
    public Integer modify(StandardApi standardApi) {
        return standardApiStore.modify(standardApi);
    }

    @Override
    public Integer remove(StandardApi standardApi) {
        return standardApiStore.remove(standardApi);
    }

    @Override
    public Integer bulk(StandardApi standardApi) {
        return standardApiStore.bulk(standardApi);
    }

    @Override
    public Integer towInsert(List<StandardApi> list1, List<StandardApi> list2, List<String> foreignKey) {
        return standardApiStore.towInsert(list1, list2, foreignKey);
    }
}
