package co.kr.msa.store.jpo;

import co.kr.msa.entity.StandardApi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.ibatis.type.Alias;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Alias("standardApiJpo")
public class StandardApiJpo {

    private String tableNm;

    //auto generation id
    private Integer id;

    //SELECT
    private List<String> fields;

    //INSERT, UPDATE
    private Map<String, Object> values;

    //WHERE
    private Map<String, Object> equal;
    private Map<String, Object> like;
    private Map<String, List<String>> between;
    private Map<String, List<String>> inCondition;
    private Map<String, String> is;

    //ORDER BY
    private Map<String, String> orderBy;

    //PAGINATION
    private Integer offset; // offset
    private Integer limit; // limit
    private Integer page; // page

    public StandardApiJpo(StandardApi standardApi) {
        if(standardApi != null) {
            BeanUtils.copyProperties(standardApi, this);
        }
    }

}