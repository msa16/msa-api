package co.kr.msa.store;

import co.kr.msa.entity.StandardApi;
import co.kr.msa.store.jpo.StandardApiJpo;
import co.kr.msa.store.mapper.StandardApiMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class StandardApiMapperStore implements StandardApiStore {

    private final StandardApiMapper standardApiMapper;

    public StandardApiMapperStore(StandardApiMapper standardApiMapper) {
        this.standardApiMapper = standardApiMapper;
    }

    @Override
    public List<Map<String, Object>> selectList(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        return standardApiMapper.selectList(jpo);
    }

    @Override
    public Map<String, Object> selectOne(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        return standardApiMapper.selectOne(jpo);
    }

    @Override
    public Integer count(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        return standardApiMapper.count(jpo);
    }

    @Override
    public Integer create(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        int count = standardApiMapper.create(jpo);
        if(count > 0 && jpo.getId() != null){
            standardApi.setId(jpo.getId());
            count = jpo.getId();
        }

        return count;
    }

    @Override
    public Integer modify(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        return standardApiMapper.modify(jpo);
    }

    @Override
    public Integer remove(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        return standardApiMapper.remove(jpo);
    }

    @Override
    public Integer bulk(StandardApi standardApi) {
        StandardApiJpo jpo = new StandardApiJpo(standardApi);
        return standardApiMapper.bulk(jpo);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public Integer towInsert(List<StandardApi> list1, List<StandardApi> list2, List<String> foreignKey) {
        int result = 0;
        for(int i = 0 ; i < list1.size() ; i++){
            int idx = create(list1.get(i));

            for(String fk : foreignKey){
                if(list1.get(i).getValues().containsKey(fk)){
                    list2.get(i).addValues(fk, list1.get(i).getValues().get(fk));
                } else {
                    list2.get(i).addValues(fk, idx);
                }
            }

            result += create(list2.get(i));
        }

        return result;
    }
}
