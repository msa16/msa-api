package co.kr.msa.store.mapper;

import co.kr.msa.store.jpo.StandardApiJpo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface StandardApiMapper {
    List<Map<String, Object>> selectList(StandardApiJpo jpo);

    Map<String, Object> selectOne(StandardApiJpo jpo);

    Integer count(StandardApiJpo jpo);

    Integer create(StandardApiJpo jpo);

    Integer modify(StandardApiJpo jpo);

    Integer remove(StandardApiJpo jpo);

    Integer bulk(StandardApiJpo jpo);
}
